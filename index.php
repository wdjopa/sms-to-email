<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';
require __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';

try {
    // Récupérer les données du destinataire via POST ou GET
    $toEmail = $_REQUEST['toEmail'] ?? 'wdjopa@lamater.tech'; // Remplacez 'default@example.com' par une valeur par défaut ou gérez l'erreur
    $subject = $_REQUEST['subject'] ?? 'Sujet par défaut';
    $message = $_REQUEST['message'] ?? 'Message par défaut';
$toName= $_REQUEST["toName"] ?? "Wilfried";
    // Récupérer les informations Gmail depuis les variables d'environnement
    $gmailUser = $_ENV['GMAIL_USER'];
    $gmailPass = $_ENV['GMAIL_PASS'];

    // Configuration du serveur
    $mail->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->Host       = 'smtp.gmail.com';
    $mail->SMTPAuth   = true;
    $mail->Username   = $gmailUser;
    $mail->Password   = $gmailPass;
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port       = 587;

    // Destinataires
    $mail->setFrom($gmailUser, 'SMS To Email');
    $mail->addAddress($toEmail, $toName);

    // Contenu
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body    = $message;
    $mail->AltBody = strip_tags($message);

    $mail->send();
    echo 'Message envoyé';
} catch (Exception $e) {
    echo "Le message n'a pas pu être envoyé. Mailer Error: {$mail->ErrorInfo}";
}

